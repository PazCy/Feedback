package com.feedback;

import android.util.Log;

import com.facebook.react.modules.network.OkHttpClientProvider;
import com.facebook.react.modules.network.ReactCookieJarContainer;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.CertificatePinner;

public class OkHttpCertPin {
  private static String hostname = "*.your.service.com";
  private static final String TAG = "OkHttpCertPin";

  public static OkHttpClient extend(OkHttpClient currentClient){
    try {
      CertificatePinner certificatePinner = new CertificatePinner.Builder()
        .add(hostname, "sha256/MIID3zCCAsegAwIBAgIJAKRV9baJE2vBMA0GCSqGSIb3DQEBCwUAMIGFMQswCQYDVQQGEwJJTDEPMA0GA1UECAwGSXNyYWVsMRAwDgYDVQQHDAdIYVphZm9uMQwwCgYDVQQKDANQJlkxDzANBgNVBAsMBlNlcnZlcjERMA8GA1UEAwwIRkVFREJBQ0sxITAfBgkqhkiG9w0BCQEWEnR2LnNsaWRlQGdtYWlsLmNvbTAeFw0xODA0MTgxMzIxMDVaFw0yODA0MTUxMzIxMDVaMIGFMQswCQYDVQQGEwJJTDEPMA0GA1UECAwGSXNyYWVsMRAwDgYDVQQHDAdIYVphZm9uMQwwCgYDVQQKDANQJlkxDzANBgNVBAsMBlNlcnZlcjERMA8GA1UEAwwIRkVFREJBQ0sxITAfBgkqhkiG9w0BCQEWEnR2LnNsaWRlQGdtYWlsLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMFjXZzrJUIGrcFZU3ZzgpoQtjYTz3m1AbHnlOs4zX2XPxm2RYHNCb9jMQ+UZoSkFQsrkgMDf1Os2mG1RZz/X4UZFwTNgZ3DFs2+WDBYMVWNM0JYW2m9zRvOn5l6LhG1c4d1EU7C2lPWTWuSCMEP1JgP4Fh7ruX8OY/s/oGzKA4qXXl0wcuMJvh+oWtMfYXdAOjLu9Gnw/juEAqJu9lfenrHU1q4atKRAp4d+tPn0Duzf5F8S9nWXV+spq1mW28pM2e9PO1LIsFPQfB5ljZ4397BvRIOj7HDDDc89i37428c6dDNl1wLNWGRJzr8fqwds3jqatnpM8bW5MXJEjgbHA0CAwEAAaNQME4wHQYDVR0OBBYEFP8/9xgFIPgYRwC6V+JN0DqPTDj1MB8GA1UdIwQYMBaAFP8/9xgFIPgYRwC6V+JN0DqPTDj1MAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAAU45QD4U8MaeF4dX4LW5Tbgaw0v43DWbIvzkEZmaDd0sAKyzT2OOUUNsW0vHLvyWfXuS406b1dCdDWsQnwIL50HVz0WQeG++11GjCGBbxWJg3kN74spa2OW6O9WystXc7GPTQ4lsmMFsJGXoN1Iac1ygHNnd0sd+xvQkEWJsGHVygMd400NpVnnwRP74NPx4TMM1ecjBVMjavdG1L9/WjvDrJ3dCOUbEX6gqB64YJSQUc7rl7Zbki3fmDddQsElKiPDU0B7SZiCtQ8LcTgkQ0WENdB/UP43YZG+9e9uPILORLNAGenbsYVQq6eF1vYNJ+A4ht5Wyol7jPmgkb8sh90=")
        .build();
      return currentClient.newBuilder().certificatePinner(certificatePinner).build();
    } catch (Exception e) {
      Log.e(TAG, e.getMessage());
    }
    return currentClient;
  }
}
