import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
      alignItems: 'center',
      flex: 1,
      paddingTop: Dimensions.get('window').height * (35/1080),
      backgroundColor: '#FAFAFA'
  },
  
  title: {
    fontSize: 25,
    fontWeight: '500',
    color: globals.mainThemeColor
  },

  errorMessage: {
    paddingTop: 25,
    fontSize: 25,
    fontWeight: '500',
    color: '#F50057'
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
    backgroundColor: globals.mainThemeColor,
    width: Dimensions.get('window').width * (490/1080),
    height: Dimensions.get('window').height * (130/1920),
  },

  buttonsContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: Dimensions.get('window').height * (250/1920),
  },

  buttonContainer: {
    paddingBottom: Dimensions.get('window').height * (30/1920),
  },

  textInput: {
    width: Dimensions.get('window').width * (550/1080),
  },

  signUpInputsContainer: {
    paddingTop: Dimensions.get('window').height * (150/1920),
    paddingBottom: Dimensions.get('window').height * (200/1920)
  },

  inputField : {
    paddingBottom: Dimensions.get('window').height * (40/1920),
  },

  subtitle: {
    fontSize: 15,
    fontWeight: '500',
    color: '#F06292'
  },
})

