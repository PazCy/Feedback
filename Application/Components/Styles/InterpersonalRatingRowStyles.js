import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
      flexDirection: 'row',
      backgroundColor: '#FAFAFA',
  },

  rating: {
    paddingLeft: Dimensions.get('window').width * (100/1080),
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#FAFAFA'
  },

  averageText: {
    color: '#FF5722',
    fontWeight: '500',
  },

  positiveText: {
    color: '#4CAF50',
    fontWeight: '500',
  },

  negativeText: {
    color: '#F44336',
    fontWeight: '500',
  },
})
