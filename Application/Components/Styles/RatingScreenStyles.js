import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
      alignItems: 'center',
      flex: 1,
      backgroundColor: '#FAFAFA',
  },

  scrollViewContainer: {
    paddingTop: Dimensions.get('window').height * (35/1080),
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#FAFAFA',
},

  title: {
    fontSize: 20,
    fontWeight: '500',
    color: globals.mainThemeColor,
    paddingBottom: Dimensions.get('window').height * (10/1080),
    paddingTop: Dimensions.get('window').height * (10/1080),
  },
  slidersContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * (450/1080),
    paddingTop: Dimensions.get('window').height * (45/1080),
    backgroundColor: '#FAFAFA',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  slider: {
    width: Dimensions.get('window').width * (500/1080),
    paddingBottom: Dimensions.get('window').height * (45/1080),
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
    backgroundColor: globals.mainThemeColor,
    width: Dimensions.get('window').width * (490/1080),
    height: Dimensions.get('window').height * (130/1920),
  },

  buttonsContainer: {
    paddingTop: Dimensions.get('window').height * (75/1080),
  },
})
