import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
      alignItems: 'center',
      flex: 1,
      paddingTop: Dimensions.get('window').width * (200/1080),
      backgroundColor: '#FAFAFA'
  },
  
  title: {
    fontSize: 25,
    fontWeight: '500',
    color: globals.mainThemeColor
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
    backgroundColor: globals.mainThemeColor,
    width: Dimensions.get('window').width * (490/1080),
    height: Dimensions.get('window').height * (130/1920),
  },

  buttonsContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: Dimensions.get('window').height * (450/1920)
  },

  buttonContainer: {
    paddingBottom: Dimensions.get('window').height * (30/1920)
  }
})
