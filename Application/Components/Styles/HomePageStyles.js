import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
      alignItems: 'center',
      flex: 1,
      backgroundColor: '#FAFAFA',
  },

  scrollViewContainer: {
    paddingTop: Dimensions.get('window').height * (35/1080),
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#FAFAFA',
},

  peopleListContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingTop: Dimensions.get('window').height * (25/1080),
    height: Dimensions.get('window').height * (250/1080),
    backgroundColor: '#FAFAFA',
},

  profileScrollView: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    height: Dimensions.get('window').height * (200/1080),
    backgroundColor: '#FAFAFA',
  },

  peopleRatingScrollView: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'column',
    backgroundColor: '#FAFAFA',
  },


  title: {
    fontSize: 20,
    fontWeight: '500',
    color: globals.mainThemeColor,
    paddingBottom: Dimensions.get('window').height * (10/1080),
    paddingTop: Dimensions.get('window').height * (10/1080),
  },

  peopleRating: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * (350/1080),
    paddingTop: Dimensions.get('window').height * (25/1080),
    backgroundColor: '#FAFAFA',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12.5,
    backgroundColor: globals.mainThemeColor,
    width: Dimensions.get('window').width * (490/1080),
    height: Dimensions.get('window').height * (130/1920),
    margin: Dimensions.get('window').height * (5/1080),
  },

  buttonsContainer: {
    paddingTop: Dimensions.get('window').height * (75/1080),
    width: Dimensions.get('window').width,
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexGrow: 1,
    backgroundColor: '#fafafa',
    justifyContent: 'center',
  },


  joinContainer: {
    alignItems: 'center',
    flex: 1,
    paddingTop: Dimensions.get('window').height * (35/1080),
    backgroundColor: '#FAFAFA'
  },
  
  textInput: {
    width: Dimensions.get('window').width * (550/1080),
  },

  subtitle: {
    paddingTop: Dimensions.get('window').height * (35/1080),
    fontSize: 15,
    fontWeight: '500',
    color: '#F06292'
  },

})
