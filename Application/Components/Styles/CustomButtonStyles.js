import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    justifyContent: 'center', 
    alignItems: 'center'
  },

  defaultText: {
    fontSize: 17,
    fontWeight: '100',
    color: '#ffffff',
    alignSelf: 'center',
    textAlign: 'center',
  }
})