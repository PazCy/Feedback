import { StyleSheet, Dimensions } from 'react-native'
var globals = require("../Globals")

export default StyleSheet.create({
  container: {
    padding: Dimensions.get('window').height * (10/1440),
    justifyContent: 'center', 
    alignItems: 'center',
    paddingBottom: Dimensions.get('window').height * (70/2880),
  },

  imageStyle: {
    justifyContent: 'center', 
    alignItems: 'center',
    borderColor: globals.mainThemeColor,
    borderWidth: 1.5,
    borderRadius: 150,
    height: Dimensions.get('window').width * (300/1440),
    width: Dimensions.get('window').width * (300/1440),
  },
  
  nameText: {
    fontSize: 17,
    fontWeight: '500',
    color: globals.mainThemeColor,
    alignSelf: 'center',
    textAlign: 'center',
    paddingTop: Dimensions.get('window').height * (25/2880),
    
  },
})
