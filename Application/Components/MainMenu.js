import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import styles from './Styles/MainMenuStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
var globals = require("./Globals")
import { NavigationActions } from 'react-navigation'

export default class MainMenu extends React.Component {
    
    static navigationOptions = {
        header: null
      };

    constructor(props){
        super(props)
        
        globals.navigation = this.props.navigation;
        this.goToSignIn = this.goToSignIn.bind(this)
        this.goToSignUp = this.goToSignUp.bind(this)
        this.devOption = this.devOption.bind(this)

        var ls = require('react-native-local-storage')
        ls.get('SSID').then((data)=> 
        {
          if(data != null){
              globals.SSID = data.ssid
              globals.resetStackAndGoTo(this.props.navigation, 'HomePage')
          }
        })
    }

    devOption()
    {
        this.props.navigation.navigate('HomePage')
    }

    goToSignUp()
    {
        this.props.navigation.navigate('SignUp')
    }

    goToSignIn()
    {
        this.props.navigation.navigate('SignIn')
    }
    

    render() {
      return (
          <View style={styles.container}>
              <Text style={styles.title}>Welcome</Text>

              <View style={styles.buttonsContainer}>
                <CustomButton onPress={this.goToSignIn} buttonContainer={styles.buttonContainer} buttonStyle={styles.button} text='Sign in'/>
                <CustomButton onPress={this.goToSignUp} buttonContainer={styles.buttonContainer} buttonStyle={styles.button} text='Sign up'/>
                {globals.isDebug ? <CustomButton onPress={this.devOption} buttonStyle={styles.button} text='Fun stuff'/> : null}
              </View>
          </View>
      );
    }
  }
  
  
