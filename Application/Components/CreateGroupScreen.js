import React from 'react';
import { StyleSheet, Text, View, TextInput, Alert, Clipboard } from 'react-native';
import styles from './Styles/SignInStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
var networkConst = require("./NetworkingConst.js")
var globals = require("./Globals")

export default class SignIn extends React.Component {
    
    static navigationOptions = {
        title: 'Create a Group'
      };

    constructor(props){
        super(props)
        this.state = {groupName: ''}
        this.createGroup = this.createGroup.bind(this)
    }

    createGroup()
    {
        fetch(networkConst.host + "/CreateGroup", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                ssid: globals.SSID,
                groupName: this.state.groupName
              }),
        })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(responseJson => 
        {
            if(responseJson["Successful"])
            {
                Alert.alert(
                    'Group Created!',
                    'Access code : ' + responseJson["accessCode"],
                    [
                      {text: 'OK'},
                      {text: 'Copy to clipboard', onPress: ()=>{Clipboard.setString(responseJson["accessCode"]);}}
                    ],
                    { cancelable: false }
                );
                this.props.navigation.navigate('HomePage')
                return
            }
                
            Alert.alert(
                'Error',
                responseJson["ErrMsg"] +"\nGroup was not created, please try again later",
                [
                  {text: 'OK'},
                ],
                { cancelable: false }
            );
        })
    }

    render() {
      return (
          <View style={styles.container}>
              <Text style={styles.title}>Create a Group</Text>

              <Text style={styles.errorMessage}>{this.state.errorMsg}</Text>

              <View style={styles.signUpInputsContainer}>
                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>Group Name</Text>
                    <TextInput style={styles.textInput} onChangeText={(text) => this.setState({groupName: text})} />
                </View>
              </View>

              <View style={styles.buttonsContainer}>
                <CustomButton onPress={this.createGroup} buttonStyle={styles.button} text='Create Group'/>
              </View>
          </View>
      );
    }
  }
  
  