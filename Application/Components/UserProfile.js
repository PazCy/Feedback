export default class UserProfile {
    constructor(firstName, lastName, UID, profilePic)
    {
        this._firstName = firstName;
        this._lastName = lastName;
        this._UID = UID;
        this._profilePic = profilePic;

        this.getFullName = this.getFullName.bind(this)
        this.getUID = this.getUID.bind(this)
        this.getProfilePic = this.getProfilePic.bind(this)
        this.getFirstName = this.getFirstName.bind(this)
        this.getLastName = this.getLastName.bind(this)
    }

    getFirstName()
    {
        return this._firstName;
    }

    getLastName()
    {
        return this._lastName;
    }

    getFullName()
    {
        return this._firstName + " " + this._lastName;
    }

    getUID()
    {
        return this._UID;
    }

    getProfilePic()
    {
        return this._profilePic;
    }
}