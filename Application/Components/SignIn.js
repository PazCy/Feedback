import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import styles from './Styles/SignInStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
import RNFetchBlob from 'react-native-fetch-blob'
var networkConst = require("./NetworkingConst.js")
var globals = require("./Globals")

export default class SignIn extends React.Component {
    
    static navigationOptions = {
        title: 'Sign in'
      };

    constructor(props){
        super(props)
        this.state = {
                        email: '',
                        password: '',
                        ErrMsg: ''
                     }

        this.signUp = this.signUp.bind(this)
        this.saveSSID = this.saveSSID.bind(this)
    }

    signUp()
    {
        fetch(networkConst.host + "/SignIn", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email,
                passHash: this.state.password
              }),
        })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(responseJson => 
        {
            if(responseJson["Successful"])
            {
                this.saveSSID(responseJson["ssid"])
                globals.resetStackAndGoTo(this.props.navigation, 'HomePage')
                return
            }
                
            this.setState({errorMsg: responseJson["ErrMsg"]})
        })
    }

    saveSSID(ssid)
    {
        globals.SSID = ssid
        var localStorage = require('react-native-local-storage')
        localStorage.save('SSID', {ssid: ssid})
    }

    render() {
      return (
          <View style={styles.container}>
              <Text style={styles.title}>Sign In</Text>

              <Text style={styles.errorMessage}>{this.state.errorMsg}</Text>

              <View style={styles.signUpInputsContainer}>
                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>E-Mail</Text>
                    <TextInput style={styles.textInput} onChangeText={(text) => this.setState({email: text})} />
                </View>

                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>Password</Text>
                    <TextInput style={styles.textInput} secureTextEntry={true} onChangeText={(text) => this.setState({password: text})} />
                </View>
              </View>

              <View style={styles.buttonsContainer}>
                <CustomButton onPress={this.signUp} buttonStyle={styles.button} text='Sign in'/>
              </View>
          </View>
      );
    }
  }
  
  
