import React from 'react';
import { StyleSheet, Text, View, Image, Slider, Alert, ScrollView, Icon} from 'react-native';
import styles from './Styles/RatingScreenStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
import ProfileCircle from './ProfileCircle';
import { NavigationActions } from 'react-navigation'
var globals = require("./Globals")
var networkConst = require("./NetworkingConst.js")

export default class RatingScreen extends React.Component {
    
    static navigationOptions = {
        title: 'Rating'
      };

    constructor(props){
        super(props)
        this.state = {
            userProfile: this.props.navigation.state.params.userProfile,
            trust: 0,
            clarity: 0,
            care: 0
        }

        this.resetNavigationStackAndGoToHomePage = this.resetNavigationStackAndGoToHomePage.bind(this)
        this.goToProfile = this.goToProfile.bind(this)
        this.rate = this.rate.bind(this)
    }

    goToProfile(id){
        console.warn(id)
    }

    resetNavigationStackAndGoToHomePage()
	{
		this.props.navigation.dispatch(NavigationActions.reset({
			index: 0,
			key: null,
			actions: [NavigationActions.navigate({ routeName: 'HomePage' })]
		}))
    }
    
    rate()
    {
          fetch(networkConst.host + "/Rate", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                ssid: globals.SSID,
                UID: this.state.userProfile.getUID(),
                clarityScore: this.state.clarity,
                careScore: this.state.care,
                trustScore: this.state.trust,
              }),
        })
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(responseJson => 
        {
            if(responseJson["Successful"])
            {
                Alert.alert(
                    'Rated!',
                    'Thanks for rating :)',
                    [
                      {text: 'OK'},
                    ],
                    { cancelable: false }
                );
                this.resetNavigationStackAndGoToHomePage()
                return
            }
            else if(responseJson["Successful"] = -1)
            {
                // If the SSID doesn't exist (code -1), remove the SSID
                globals.clearSSID()
                
                // Reset the navigation stack and go to the mainmenu
                globals.resetStackAndGoTo(this.props.navigation,'MainMenu')
            }
            Alert.alert(
                'Error',
                responseJson["ErrMsg"] +"\nYour rating was not saved, please try again later",
                [
                  {text: 'OK'},
                ],
                { cancelable: false }
            );
        })
    }

    render() {
      return (
          <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.scrollViewContainer}>
                <ProfileCircle user={this.state.userProfile}/>
                <View style={styles.slidersContainer}>
                    <Text style={styles.title}>Trust</Text>
                    <Slider style={styles.slider} thumbTintColor={globals.mainThemeColor} minimumTrackTintColor={globals.mainThemeColor} maximumValue={100} onValueChange={(value) => this.setState({trust: value})}/>
                    <Text style={styles.title}>Clarity</Text>
                    <Slider style={styles.slider} thumbTintColor={globals.mainThemeColor} minimumTrackTintColor={globals.mainThemeColor} maximumValue={100} onValueChange={(value) => this.setState({clarity: value})}/>
                    <Text style={styles.title}>Care</Text>
                    <Slider style={styles.slider} thumbTintColor={globals.mainThemeColor} minimumTrackTintColor={globals.mainThemeColor} maximumValue={100} onValueChange={(value) => this.setState({care: value})}/>
                </View>
                <View style={styles.buttonsContainer}>
                    <CustomButton onPress={this.rate} buttonStyle={styles.button} text='Rate'/>
                </View>
            </ScrollView>
          </View>
      );
    }
  }
  
  
