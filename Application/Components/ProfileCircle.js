import React, { Component } from 'react'
import { View, TouchableHighlight, Text, Image} from 'react-native'
import styles from './Styles/ProfileCircleStyles'
import UserProfile from './UserProfile'

export default class CustomButton extends Component {
  constructor(props){
    super(props)
    this.state = {
      onPressEvent: this.props.onPress,
      fullName: this.props.user.getFullName(),
      UID: this.props.user.getUID(),
      profilePic: this.props.user.getProfilePic(),
      userProfile: this.props.user
    };

    this.press = this.pressEvent.bind(this)
    this.getUserProfile = this.getUserProfile.bind(this)
  }

  getUserProfile()
  {
    return this.state.userProfile;
  }

  pressEvent(){
    this.props.onPress()
  }

  render () {
  return(
    <TouchableHighlight activeOpacity={0.9} onPress={this.props.onPress ? this.press : null}>
      <View style={styles.container}>
         <Image style={styles.imageStyle} source={this.state.profilePic}/>
         <Text style={styles.nameText}>{this.state.fullName}</Text>
      </View>
    </TouchableHighlight>
  )
  }

}
