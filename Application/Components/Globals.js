import { NavigationActions } from 'react-navigation'
import { StackNavigator } from 'react-navigation';
module.exports = {
    isDebug: false,
    SSID: undefined,
    inGroup: undefined,
    firstName: undefined,
    lastName: undefined,
    picture: undefined,
    groupName: undefined,
    uid: undefined,
    mainThemeColor: '#EC407A',
    navigation: undefined,
    resetStackAndGoTo: (navigation, goto)=>
    {
            navigation.dispatch(NavigationActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: goto })]
            }))
    },
    clearSSID: ()=>
    {
        // Clear the SSID from the cache and the global variable
	    this.SSID = undefined
	    var localStorage = require('react-native-local-storage')
	    localStorage.remove('SSID')
    }
}
