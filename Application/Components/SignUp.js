import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import styles from './Styles/SignUpStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
var networkConst = require("./NetworkingConst.js")
var globals = require("./Globals")


export default class SignUp extends React.Component {
    
    static navigationOptions = {
        title: 'Sign up'
      };

    constructor(props){
        super(props)
        this.state = {
                        email: '',
                        firstName: '',
                        lastName: '',
                        password: ''
                    }

        this.signUp = this.signUp.bind(this)
        this.saveSSID = this.saveSSID.bind(this)
    }

    signUp()
    {
        fetch(networkConst.host + "/SignUp", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                passHash: this.state.password
              }),
        })
        .then(response => response.json())
        .then(responseJson => 
        {
            if(responseJson["Successful"])
            {
                this.saveSSID(responseJson["ssid"])
                globals.resetStackAndGoTo(this.props.navigation, 'HomePage')
                return
            }
            this.setState({errorMsg: responseJson["ErrMsg"]})
        })
    }

    saveSSID(ssid)
    {
        globals.SSID = ssid
        var localStorage = require('react-native-local-storage')
        localStorage.save('SSID', {ssid: ssid})
    }

    render() {
        //TODO : Add if logged in, dont show sign in/up scren
      return (
          <View style={styles.container}>
              <Text style={styles.title}>Sign Up</Text>

              <Text style={styles.errorMessage}>{this.state.errorMsg}</Text>
            
              <View style={styles.signUpInputsContainer}>
                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>E-Mail</Text>
                    <TextInput style={styles.textInput} onChangeText={(text) => this.setState({email: text})} />
                </View>

                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>First Name</Text>
                    <TextInput style={styles.textInput} onChangeText={(text) => this.setState({firstName: text})} />
                </View>

                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>Last Name</Text>
                    <TextInput style={styles.textInput} onChangeText={(text) => this.setState({lastName: text})} />
                </View>

                <View style={styles.inputField}>
                    <Text style={styles.subtitle}>Password</Text>
                    <TextInput style={styles.textInput} secureTextEntry={true} onChangeText={(text) => this.setState({password: text})} />
                </View>
              </View>

              <View style={styles.buttonsContainer}>
                <CustomButton onPress={this.signUp} buttonStyle={styles.button} text='Sign up'/>
              </View>
          </View>
      );
    }
  }
  
  
