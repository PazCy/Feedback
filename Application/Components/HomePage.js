import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, Alert, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './Styles/HomePageStyles'
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';
import ProfileCircle from './ProfileCircle';
import InterpersonalRatingRow from './InterpersonalRatingRow';
import UserProfile from './UserProfile'
import { NavigationActions } from 'react-navigation'
var networkConst = require("./NetworkingConst.js")
var globals = require("./Globals")


export default class HomePage extends React.Component {
	
	static navigationOptions = {
		header: null
	};
	
	constructor(props){
		super(props)
		
		this.state = {
			accessCode: null, 
			loaded: 0,
			ratingList: null,
			inGroup: globals.inGroup
		}
		
		this.profiles = []
		this.ratings = []
		this.gotoGroupCreation = this.gotoGroupCreation.bind(this)
		this.joinGroup = this.joinGroup.bind(this)
		this.getProfiles = this.getProfiles.bind(this)
		this.getRecommendedPeople = this.getRecommendedPeople.bind(this)
		this.getRatingInfo = this.getRatingInfo.bind(this)
		this.getPersonalInfo = this.getPersonalInfo.bind(this)
		this.navigateToProfile = this.navigateToProfile.bind(this)
		
	}
	
	componentWillMount()
	{
		this.getPersonalInfo()
		this.forceUpdate()
	}
	
	navigateToProfile(profile)
	{
		this.props.navigation.navigate('RatingScreen', {userProfile: profile})
	}
	
	getPersonalInfo()
	{
		fetch(networkConst.host + "/GetPersonalInfo", {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				ssid: globals.SSID,
			}),
		})
		.then(response => {
			console.log(response)
			return response.json()
		})
		.then(responseJson => 
			{	
				if(responseJson["Successful"])
				{
					globals.firstName = responseJson["firstName"]
					globals.lastName = responseJson["lastName"]
					globals.uid = responseJson["UID"]
					this.setState({inGroup: responseJson["inGroup"]})
					globals.inGroup = responseJson["inGroup"]
					globals.picture = responseJson["Picture"]
					globals.groupName = responseJson["groupName"]
					return
				}
				else if(responseJson["Successful"] == -1)
				{
					// If the SSID doesn't exist (code -1), remove the SSID
					globals.clearSSID()
					
					// Reset the navigation stack and go to the mainmenu
					globals.resetStackAndGoTo(this.props.navigation,'MainMenu')
				}
			}).then(this.getRecommendedPeople())
		}
		
			
			getRecommendedPeople()
			{
				fetch(networkConst.host + "/RecommendedPeople", {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						ssid: globals.SSID,
					}),
				})
				.then(response => {
					console.log(response)
					return response.json()
				})
				.then(responseJson => 
					{
						
						if(responseJson["Successful"])
						{               
							responseJson["profiles"].map((profile) => 
							{
								profileUID = profile[0]
								profileFirstname = profile[1]
								profileLastname = profile[2]
								profilePic = profile[3]
								
								if(profilePic == null)
								profilePic = require('./download.png')
								
								userProf = new UserProfile(profileFirstname, profileLastname, profileUID, profilePic)
								this.getRatingInfo(profileUID, userProf)
								this.profiles.push(userProf)
							});
						}
						else if(responseJson["Successful"] == -1)
						{
							// If the SSID doesn't exist (code -1), remove the SSID
							globals.clearSSID()
							
							// Reset the navigation stack and go to the mainmenu
							globals.resetStackAndGoTo(globals.navigation,'MainMenu')
						}
					}).then(()=>{
						this.setState({ratingList: this.ratings})
						this.setState({loaded: 1})
					})
				}

				getRatingInfo(uid, userProfile)
				{
					fetch(networkConst.host + "/GetMyRatings", {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							ssid: globals.SSID,
							UID: uid,
						}),
					})
					.then(response => {
						console.log(response)
						return response.json()
					})
					.then(responseJson => 
						{
							
							if(responseJson["Successful"])
							{               
								trust = responseJson["trustScore"]
								clarity = responseJson["clarityScore"]
								care = responseJson["careScore"]
		
								if(trust == null || clarity == null || care == null)
									return
								
								this.ratings.push(<InterpersonalRatingRow key={userProfile.getUID()} clarity={clarity} trust={trust} care={care} profile={userProfile}/>)
								this.setState({ratingList: this.ratings})
								return
							}
							else if(responseJson["Successful"] == -1)
							{
								// If the SSID doesn't exist (code -1), remove the SSID
								globals.clearSSID()
								
								// Reset the navigation stack and go to the mainmenu
								globals.resetStackAndGoTo(this.props.navigation,'MainMenu')
							}
						})
					}
				
				gotoGroupCreation()
				{
					this.props.navigation.navigate('CreateGroupScreen')
				}
				
				joinGroup()
				{
					fetch(networkConst.host + "/JoinGroup", {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							ssid: globals.SSID,
							accessCode: this.state.accessCode
						}),
					})
					.then(response => {
						console.log(response)
						return response.json()
					})
					.then(responseJson => 
						{
							
							if(responseJson["Successful"])
							{
								globals.resetStackAndGoTo(this.props.navigation, 'HomePage')
								return
							}
							else if(responseJson["Successful"] == -1)
							{
								// If the SSID doesn't exist (code -1), remove the SSID
								globals.clearSSID()
								
								// Reset the navigation stack and go to the mainmenu
								globals.resetStackAndGoTo(this.props.navigation,'MainMenu')
							}
							
							Alert.alert(
								'Error',
								responseJson["ErrMsg"] +"\nCould not join group, please try again later",
								[
									{text: 'OK'},
								],
								{ cancelable: false }
							);
						})
					}
					
					getProfiles()
					{  
						return this.profiles.map(
							function(profile, index)
							{
								return(<ProfileCircle key={index} onPress={()=>this.navigateToProfile(profile)} user={profile}/>)
							}
						, this)
					}

					signOut()
					{
						fetch(networkConst.host + "/SignOut", {
							method: 'POST',
							headers: {
								Accept: 'application/json',
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({
								ssid: globals.SSID,
							}),
						})
						globals.clearSSID(); 
						globals.resetStackAndGoTo(globals.navigation,'MainMenu');
					}
					
					render() 
					{
						if (!this.state.loaded || this.state.ratingList == null) {
							return (
							  <ActivityIndicator
								animating={true}
								style={{alignItems: 'center'}}
								size="large"
							  />
							);
						  }
						
						if(this.state.inGroup)
						{
							return (
								<View style={styles.container}>
									<ScrollView contentContainerStyle={styles.scrollViewContainer}>
										<Text style={styles.title}>Rate Someone</Text>
										<View style={styles.peopleListContainer}>
											<ScrollView style={styles.profileScrollView} horizontal={true} showsHorizontalScrollIndicator={false} alwaysBounceHorizontal={false}>
												{this.getProfiles()}
											</ScrollView>
										</View>

										<Text style={styles.title}>Interpersonal Ratings</Text>
										<View style={styles.peopleRating}>
											<ScrollView style={styles.peopleRatingScrollView} pagingEnabled={true} showsVerticalScrollIndicator={false} alwaysBounceVertical={false}>
												{this.state.ratingList}
											</ScrollView>
										</View>
								
										<View style={styles.buttonsContainer}>
											<CustomButton onPress={this.gotoGroupCreation} buttonStyle={styles.button} text='Create Group'/>
											<CustomButton onPress={()=>{this.setState({inGroup: 0})}} buttonStyle={styles.button} text='Join Group'/>
											<CustomButton onPress={this.signOut} buttonStyle={styles.button} text='Sign Out'/>
										</View>
									</ScrollView>
								</View>
							);
						}
						else
						{
							return(
								<View style={styles.joinContainer}>
									<Text style={styles.title}>Join a Group</Text>
									<Text style={styles.errorMessage}>{this.state.errorMsg}</Text>
								
									<View style={styles.signUpInputsContainer}>
										<View style={styles.inputField}>
											<Text style={styles.subtitle}>Access Code</Text>
												<TextInput style={styles.textInput} onChangeText={(text) => this.setState({accessCode: text})} />
										</View>
									</View>
								
									<View style={styles.buttonsContainer}>
										<CustomButton onPress={this.joinGroup} buttonStyle={styles.button} text='Join Group'/>
									</View>
								</View>
							);
						}
					}
				}
				
				
