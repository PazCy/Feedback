import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import styles from './Styles/InterpersonalRatingRowStyles'
import ProfileCircle from "./ProfileCircle"
import CustomButton from './CustomButton'
import { StackNavigator } from 'react-navigation';

export default class InterpersonalRatingRow extends React.Component {
    static navigationOptions = {
        header: null
      };

    constructor(props){
        super(props)
    }

    getTextStyleByPrecent(matchPrecent)
    {
        if(matchPrecent < 49)
            return styles.negativeText
        else if(matchPrecent >= 49 && matchPrecent <= 51)
            return styles.averageText
        else
            return styles.positiveText
    }
    render() {
        //TODO : Add if logged in, dont show sign in/up scren
      return (
        <View style={styles.container}>
            {<ProfileCircle user={this.props.profile}/>}
            <View style={styles.rating}>
                <Text style={this.getTextStyleByPrecent(this.props.trust)}>Trust : {this.props.trust}% match</Text>
                <Text style={this.getTextStyleByPrecent(this.props.clarity)}>Clarity : {this.props.clarity}% match</Text>
                <Text style={this.getTextStyleByPrecent(this.props.care)}>Care : {this.props.care}% match</Text>
            </View>
        </View>
      );
    }
}