import React, { Component } from 'react'
import { View, TouchableHighlight, Text} from 'react-native'
import styles from './Styles/CustomButtonStyles'

export default class CustomButton extends Component {
  constructor(props){
    super(props)
  }

  render () {
  return(
    <View style={this.props.buttonContainer}>
        <View style={styles.container}>
            <TouchableHighlight style={this.props.buttonStyle} onPress={this.props.onPress}>
                <Text style={this.props.buttonTextStyle ? this.props.buttonTextStyle : styles.defaultText}>{this.props.text}</Text>
            </TouchableHighlight>
        </View>
    </View>
  )
  }

}
