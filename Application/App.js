import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainMenu from './Components/MainMenu'
import SignUp from './Components/SignUp'
import SignIn from './Components/SignIn'
import HomePage from './Components/HomePage'
import CreateGroupScreen from './Components/CreateGroupScreen'
import RatingScreen from './Components/RatingScreen'
import { StackNavigator } from 'react-navigation'

const App = StackNavigator({
  MainMenu: { screen: MainMenu },
  SignUp: { screen: SignUp },
  SignIn: { screen: SignIn },
  HomePage: { screen: HomePage },
  RatingScreen: { screen: RatingScreen },
  CreateGroupScreen: { screen: CreateGroupScreen },
});

export default App;

