from DBWrapper import addRate, getUserIDBySSID, getGIDbyUID
from CheckSSID import *

"""
The handle_Rate() function
receives: {ssid, UID (of rated), ClarityScore, CareScore, TrustScore}
EXPLAIN RATE LOGIC HERE
"""

def handle_Rate(postParams, postReplyCallback):
	
	try:
		ssid = postParams["ssid"]
		#if checkSSID(ssid) == False:
		#	return
		
		raterUID = getUserIDBySSID(ssid)
		ratedUID = postParams["UID"]
		clarityScore = postParams["clarityScore"]
		careScore = postParams["careScore"]
		trustScore = postParams["trustScore"]
		
		# Checking if two UIDs are on the same group
		if getGIDbyUID(raterUID) != getGIDbyUID(ratedUID):
			postReplyCallback(0, None, "User is not in your group")
			return
		
		# Adding rate		
		addRate(raterUID, ratedUID, clarityScore, careScore, trustScore)
		postReplyCallback(1, None, "")
		
	except Exception as e:
		print("Rate.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		
