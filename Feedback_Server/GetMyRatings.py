from DBWrapper import *
from CheckSSID import *

"""
The handle_GetMyRatings function receives ssid and uid of another user
and sends a JSON list of the all-time average scores of Trust,Clarity and care between you and the other user
"""

def handle_GetMyRatings(postParams, postReplyCallback):
	
	try:
		ssid = postParams["ssid"]
		#if checkSSID(ssid) == False:
		#	return
			
		otherUID = postParams["UID"]
		
		# Getting the user's UID
		uid = getUserIDBySSID(ssid)
			
		# Validating UID
		if uid == None:
			postReplyCallback(0, None, "Hmmh... you do not exist :O")
			return
		 
		# Getting the scores
		scores = list(getRatingScores(uid, otherUID))
			
		# Returning
		print(scores)
		postReplyCallback(1, {"clarityScore": scores[0], "careScore": scores[1], "trustScore": scores[2]}, "")
			
	except Exception as e:
		print("GetMyRatings.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		
