from DBWrapper import *
import string
import re
import random
import hashlib

def generateSalt():
	return ''.join(random.choices(string.ascii_letters + string.digits + string.punctuation, k=256))

def generateSSID():
	return ''.join(random.choices(string.ascii_letters + string.digits, k=512))

def SHA256(toSHA):
	m = hashlib.sha256()
	m.update(toSHA.encode("utf-8"))
	return m.hexdigest()

def is_SignIn_Keys_Okay(postParams):
	return ('email' in postParams
	and 'passHash' in postParams
	and len(postParams.keys()) == 2)
		
def is_SignUp_Keys_Okay(postParams):
	return ('firstName' in postParams
	and 'lastName' in postParams
	and 'email' in postParams
	and 'passHash' in postParams
	and len(postParams.keys()) == 4)

def handle_SignIn(postParams, postReplyCallback):
	'''
	Checks if user details in params appear in the users list
	Returns if successful (True/False)
	'''
	
	# Checking if valid parameters
	if is_SignIn_Keys_Okay(postParams) == False:
		postReplyCallback(0, None, "Invalid credentials")
		return False
		
	email = postParams["email"]
	passHash = postParams["passHash"]
	# Fetching user from database
	uid = getUserID(email) # Fetching from database
	
	# Validating that the user exists
	if uid == None:
		postReplyCallback(0, None, "Invalid credentials")
		return False
	# Convert UID to a STRING
	uid = str(uid)
	
	# SHA-256ing the passHash with the salt
	salt = getUserSalt(uid)
	passHash = SHA256(passHash + salt)
	
	# Validating credentials
	if passHash == getPassHash(uid):
		try:
			# Update ssid
			ssid = generateSSID()
			updateSSID(uid, ssid)
			
			# Send reply
			postReplyCallback(1, {"ssid" : ssid})
			return True
			
		except Exception as e:
			print("SignInSignUp.py: " + traceback.print_exc() + '\n')
			postReplyCallback(0, None, "Internal server error")
			return False
	else:
		postReplyCallback(0, None, "Invalid credentials")
		return False
	
def handle_SignUp(postParams, postReplyCallback):
	'''
	Inserts new user into the database unless it already exists
	Returns if succeeded (True/False)
	'''
	
	email = postParams["email"]
	firstName = postParams["firstName"]
	lastName = postParams["lastName"]
	passHash = postParams["passHash"]
	
	if areCredentialsLegal(email, firstName, lastName, passHash) == False:
		postReplyCallback(0, None, "Invalid credentials!")
		return False
	
	
	salt = generateSalt()
	ssid = generateSSID()
	
	# Checking if user already exists
	if getUserID(email) != None:
		postReplyCallback(0, None, "User already exists")
		return False

	# SHA-256ing the passHash with the salt
	passHash = SHA256(passHash + salt)

	# Adding user
	try:
		addUser(email, firstName, lastName, passHash, ssid, salt)
		postReplyCallback(1, {"ssid": ssid})
		return True
		
	except Exception as e:
		print("SignInSignUp.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		return False
		
		
def areCredentialsLegal(email, firstName, lastName, passHash):
	'''
	Checks if first name, last name and passHash are not empty. 
	Checks if email is a legal address.
	Returns true or false.
	'''
	if firstName == None or lastName == None or passHash == None:
		return False
	if firstName == '' or lastName == '' or passHash == '':
		return False
	if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
		return False
	
		
