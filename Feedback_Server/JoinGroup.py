from DBWrapper import *
from CheckSSID import *

"""
The handle_JoinGroup() function
receives: {ssid, accessCode} and adds the user
to the requested group.
"""

def handle_JoinGroup(postParams, postReplyCallback):

	try:
		ssid = postParams["ssid"]
		#if checkSSID(ssid) == False:
		#	return
		
		accessCode = postParams["accessCode"]
		
		# Getting UID and GID
		uid = getUserIDBySSID(ssid)
		gid = getGIDbyAccessCode(accessCode)
		
		# Validating UID and GID
		if uid == None or gid == None:
			postReplyCallback(0, None, "User or group not found")
			return
		
		# Removing user from already existing groups
		removeUserFromGroup(uid)
	
		# Adding user to group		
		addUserToGroup(uid, gid)	
		postReplyCallback(1, None, "")
	
	except Exception as e:
		print("JoinGroup.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
