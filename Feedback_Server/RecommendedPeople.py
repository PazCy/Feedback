from DBWrapper import *
from CheckSSID import *

"""
The handle_RecommendedPeople function receives ssid
and sends a JSON list of recommended peoples' [UID, fName, lName, picture]
"""

def handle_RecommendedPeople(postParams, postReplyCallback):
	
	try:
		ssid = postParams["ssid"]
		#if checkSSID(ssid) == False:
		#	return
		
		# Getting the user's UID
		uid = getUserIDBySSID(ssid)
		gid = getGIDbyUID(uid)
			
		# Validating GID and UID
		if gid == None or uid == None:
			postReplyCallback(0, None, "User or group not found")
			return
		
		# Getting the UIDs of the users in the same group 
		uidsInGroup = getUIDsByGID(gid, uid)
		print(uidsInGroup)
		# Creating users in group data dictionary
		recommendedPeopleData = []
		for currUID in uidsInGroup:
			currUserData = getAllPersonalInfo(currUID[0])
			recommendedPeopleData.append(list(currUserData))
			
		# Returning
		print(recommendedPeopleData)
		postReplyCallback(1, {"profiles" : recommendedPeopleData}, "")
			
	except Exception as e:
		print("RecommendedPeople.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		
