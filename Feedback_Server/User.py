from PIL import Image

"""
The User class is used to cache User 
data from the database on the server.
"""

class User():

	_email = ""
	_firstName = ""
	_lastName = ""
	_password = ""
	_passHash = ""
	_salt = ""
	_picture = Image.open('file')
	
	def __init__(self, email, firstName, lastName, password, passHash, salt, picture):
		self._email = email
		self._firstName = firstName
		self._lastName = lastName
		self._password = password
		self._passHash = passHash
		self._salt = salt
		self._picture = picture
