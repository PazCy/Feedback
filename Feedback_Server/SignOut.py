from DBWrapper import removeSSID

"""
The handle_SignOut() function receives an ssid and puts NULL where it exists
"""

def handle_SignOut(postParams, postReplyCallback):
	
	try:
		ssid = postParams["ssid"]
		removeSSID(ssid)
	
	except Exception as e:
		print("SignOut.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
