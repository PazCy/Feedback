"""
The handleMessage function will receieve a message
and call its handling function according to its type.
"""

import sys
import json
from SignInSignUp import handle_SignIn, handle_SignUp
from CreateGroup import handle_CreateGroup
from JoinGroup import handle_JoinGroup
from RecommendedPeople import handle_RecommendedPeople
from Rate import handle_Rate
from GetMyRatings import handle_GetMyRatings
from GetPersonalInfo import handle_GetPersonalInfo
from SignOut import handle_SignOut

def handleMessage(msg, postReplyCallback):

	try:
		if msg._typeID == 1:
			handle_SignIn(msg._args, postReplyCallback)
		elif msg._typeID == 2:
			handle_SignUp(msg._args, postReplyCallback)
		elif msg._typeID == 3:
			handle_RecommendedPeople(msg._args, postReplyCallback)
		elif msg._typeID == 4:
			handle_Rate(msg._args, postReplyCallback)
		elif msg._typeID == 5:
			handle_CreateGroup(msg._args, postReplyCallback)
		elif msg._typeID == 6:
			handle_JoinGroup(msg._args, postReplyCallback)
		elif msg._typeID == 7:
			handle_GetMyRatings(msg._args, postReplyCallback)
		elif msg._typeID == 8:
			handle_GetPersonalInfo(msg._args, postReplyCallback)
		elif msg._typeID == 9:
			handle_SignOut(msg._args, postReplyCallback)
		else:
			print("Unknown message type!\n")
			
	except Exception as e:
		print("MessageHandler.py:  " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error\nHANDL_MSG_FNC_ERR\n")
