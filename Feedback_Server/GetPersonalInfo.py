from DBWrapper import *
from CheckSSID import *

"""
The handle_GetPersonalInfo function receives ssid
and sends a JSON list of the user's personal info [firstName, lastName, UID, inGroup, groupName]
"""

def handle_GetPersonalInfo(postParams, postReplyCallback):
	
	try:
		ssid = postParams["ssid"]
		
		#if checkSSID(ssid, postReplyCallback) == False:
		#	return
		
		# Getting the user's UID
		uid = getUserIDBySSID(ssid)
		if uid == None:
			postReplyCallback(-1, None, "SSID expired")
			return		
		
		gid = getGIDbyUID(uid)
		groupName = ""

		# If the user is in a group, get the group name
		if gid != None:
			groupName = getGroupName(gid)

		# Get all personal info
		personalInfoArr = getAllPersonalInfo(uid)
		print(personalInfoArr)
		
		# 0 - UID, 1 - fName, 2 - lName, 3 - Picture
		firstName = personalInfoArr[1]
		lastName = personalInfoArr[2]
		picture = personalInfoArr[3]
			
		# Returning
		print(personalInfoArr)
		postReplyCallback(1, {"UID" : uid, "firstName": firstName, "lastName": lastName, "Picture": picture, "inGroup": int(gid != None), "groupName": groupName}, "")
			
	except Exception as e:
		print("GetPersonalInfo.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		
