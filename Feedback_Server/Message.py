"""
The Message class is used to translate POST parameters
received from a client into Python variables.
"""

class Message():
	_typeID = 0
	_args = {}
	
	def __init__(self, postParams, messageType):
		self._typeID = messageTypeToID(messageType)
		self._args = postParams

	def toString(self):
		return str("TypeID: " + str(self._typeID) + "\n\nParams: " + str(self._args) + "\n")
		
def messageTypeToID(messageType):
	'''
	Converts messageType string into a message ID.
	'''
	    
	typeID = 0
	
	if messageType == "SignIn":
		typeID = 1
	elif messageType == "SignUp":
		typeID = 2
	elif messageType == "RecommendedPeople":
		typeID = 3
	elif messageType == "Rate":
		typeID = 4
	elif messageType == "CreateGroup":
		typeID = 5
	elif messageType == "JoinGroup":
		typeID = 6
	elif messageType == "GetMyRatings":
		typeID = 7
	elif messageType == "GetPersonalInfo":
		typeID = 8
	elif messageType == "SignOut":
		typeID = 9
		
	return typeID
