import sqlite3
db_name_file = 'Database.db'


conn = sqlite3.connect(db_name_file)
c = conn.cursor()

def getGroupName(GID):
	c.execute('SELECT groupName FROM Groups WHERE GID = ?', (GID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None
	
def getGIDbyGroupName(groupName):
	c.execute('SELECT GID FROM Groups WHERE groupName = ?', (groupName,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getGIDbyAccessCode(accessCode):
	c.execute('SELECT GID FROM Groups WHERE accessCode = ?', (accessCode,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getGIDbyUID(UID):
	c.execute('SELECT GID FROM Roles WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getUIDsByGID(GID, UID):
	c.execute('SELECT UID FROM Roles WHERE GID = ? AND UID != ?', (GID, UID))
	fetched = c.fetchall()
	return fetched if fetched else None
	
def getFirstName(UID):
	c.execute('SELECT firstName FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getLastName(UID):
	c.execute('SELECT firstName FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getEmail(UID):
	c.execute('SELECT email FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getPassHash(UID):
	c.execute('SELECT passHash FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getAllPersonalInfo(UID):
	c.execute('SELECT UID,fName,lName,Picture FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched if fetched else None
	
def addRate(raterUID, ratedUID, clarityScore, careScore, trustScore):
	query = "INSERT INTO Ratings (raterUID, ratedUID, clarityScore, careScore, trustScore) VALUES (?, ?, ?, ?, ?)"
	c.execute(query, (raterUID, ratedUID, clarityScore, careScore, trustScore))
	conn.commit()

def addUser(email, firstName, lastName, passHash, ssid, salt):
	query = "INSERT INTO Users (email, fName, lName, passHash, ssid, salt) VALUES (?, ?, ?, ?, ?, ?)"
	c.execute(query, (email, firstName, lastName, passHash, ssid, salt))
	conn.commit()
	
def addUserToGroup(UID, GID):
	query = "INSERT INTO Roles (UID, GID) VALUES (?, ?)"
	c.execute(query, (UID, GID))
	conn.commit()
	
def removeUserFromGroup(UID):
	c.execute('DELETE FROM Roles WHERE UID = ? AND EXISTS(SELECT * FROM Roles WHERE UID = ?)', (UID, UID))
	conn.commit()

def removeUser(UID):
	c.execute('DELETE OR IGNORE FROM Users WHERE UID = ?', (UID,))
	conn.commit()

def addGroup(groupName, accessCode):
	query = "INSERT INTO Groups (groupName, accessCode) VALUES (?, ?)"
	c.execute(query, (groupName, accessCode))
	conn.commit()

def removeGroup(GID):
	c.execute('DELETE OR IGNORE FROM Group WHERE GID = ?', (GID,))
	conn.commit()

def updateSSID(UID, ssid):
	query = "UPDATE Users SET ssid = ? WHERE UID = ?"
	c.execute(query, (ssid, UID))
	conn.commit()
	
def removeSSID(ssid):
	query = "UPDATE Users SET ssid = ? WHERE ssid = ?"
	c.execute(query, (None, ssid))
	conn.commit()

def getUserSalt(UID):
	c.execute('SELECT Salt FROM Users WHERE UID = ?', (UID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getUserIDBySSID(SSID):
	c.execute('SELECT UID FROM Users WHERE ssid = ?', (SSID,))
	fetched = c.fetchone()
	return fetched[0] if fetched else None

def getUserID(email):
	c.execute('SELECT UID FROM Users WHERE email = ?', (email,))
	fetched = c.fetchone()
	return int(fetched[0]) if fetched else None

def getRatingScores(UID, otherUID):
	c.execute("SELECT CAST(ROUND(AVG(clarityScore)) as integer), CAST(ROUND(AVG(careScore)) as integer), CAST(ROUND(AVG(trustScore)) as integer) FROM Ratings WHERE (raterUID = ? AND ratedUID = ?) OR (raterUID = ? and ratedUID = ?)", (UID, otherUID, otherUID, UID))
	fetched = c.fetchone()
	return fetched if fetched else None
