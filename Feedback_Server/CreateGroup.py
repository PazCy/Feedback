from DBWrapper import *
from CheckSSID import *
import string
import random

"""
The handle_CreateGroup function receives a group name,
adds the group to the database and returns the
group's access code to the client
"""

def handle_CreateGroup(postParams, postReplyCallback):

	try:
		#if checkSSID(postParams["ssid"]) == False:
		#	return
			
		groupName = postParams["groupName"]
					
		# Creating access code - groupName + 5 random chars
		accessCode = groupName + ''.join(random.choices(string.ascii_letters + string.digits, k=5))
		
		# Adding group to database and sending reply to client
		addGroup(groupName, accessCode)
		postReplyCallback(1, {"accessCode": accessCode})

	except Exception as e:
		if "UNIQUE" in str(e):
			postReplyCallback(0, None, "Group name taken")
			return
		
		print("CreateGroup.py: " + str(e) + '\n')
		postReplyCallback(0, None, "Internal server error")
		
