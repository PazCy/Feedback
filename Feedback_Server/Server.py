import json
import sys
import ssl
from optparse import OptionParser
from http.server import BaseHTTPRequestHandler, HTTPServer
from Message import Message
from MessageHandler import handleMessage

port = 80

class RequestHandler(BaseHTTPRequestHandler):

	def do_GET(self):
		'''
		Responds to a GET message
		'''
		
		request_path = self.path
		print("\n----- Request Start ----->\n")
		print(request_path)
		print(self.headers)
		print("<----- Request End -----\n")
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()
		self.wfile.write("<html><head><title>-cough-</title><body><p><h1>USE POST!</h1></p></body></html>".encode("utf-8"))
		
	def do_POST(self):
		'''
		Responds to a POST message
		'''
		
		print("\n<----- Request Start ----->\n")
		print("Original path: " + self.path + '\n')
		
		# Parsing POST message
		postLocation = self.parse_POST_Location()
		postParamDict = self.parse_POST_Params()
		
		# Handling request
		self.handle_POST(postLocation, postParamDict)
			
		print("\n<----- Request End ----->\n")
	
	def handle_POST(self, location, postParams):
		'''
		Handles POST messages
		'''
				
		msg = Message(postParams, location) # Transffering to Message type
		print(msg.toString())
		handleMessage(msg, self.send_POST_reply) # Handling message

	def parse_POST_Params(self):
		'''
		Parses POST parameters (from the body) into a dictionary
		'''
		

		try:		
			postParams = json.loads(self.rfile.read(int(self.headers['Content-Length'])).decode("utf-8", "ignore"))
			
		except Exception as e:
			print("Server.py: " + str(e) + '\n')
			postParams = None
		
		return postParams

	def parse_POST_Location(self):
		'''
		Parses POST location (from path) into a string
		For example: localhost:8080/SignIn -> "SignIn"
		'''
		
		try:
			return self.path[str(self.path).find('/') + 1:]
		except Exception as e:
			print("Server.py: " + str(e) + '\n')
			return None

	def send_POST_reply(self, successful, parametersDict, errMsg=""):
		'''
		Sends back a JSON:
		Successful - 0/
		ErrMsg
		'''

		data = {"Successful": successful, "ErrMsg": errMsg}

		if(parametersDict != None):
			data.update(parametersDict) # Adds the parameters dictionary to the data dictionary
		
		json_data = json.dumps(data)
		
		print(json_data.encode("utf-8"))
		self.send_response(200, json_data)
		self.send_header('Content-type', 'application/json')
		self.end_headers()
		self.wfile.write(json_data.encode("utf-8"))
		#self.wfile.close()
			
def main():
	print('Listening on localhost:%s' % port)
	server = HTTPServer(('', port), RequestHandler)
	
	#server.socket = ssl.wrap_socket(server.socket, keyfile="./Cert/letsencrypt/key.pem", certfile='./Cert/letsencrypt/cert.pem', server_side=True)
	server.serve_forever()


if __name__ == "__main__":
	parser = OptionParser()
	parser.usage = ("Creates an http-server that will echo out any GET or POST parameters\n"
					"Run:\n\n"
					"   reflect")
	(options, args) = parser.parse_args()
	
	main()
