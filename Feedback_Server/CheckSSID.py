from DBWrapper import *

"""
The checkSSID() function 
"""

def safeGetUID(ssid, postReplyCallback):
	uid = getUserIDBySSID(ssid)
	if uid == None:
		postReplyCallback(-1, None, "SSID expired")
		return False
	return uid
